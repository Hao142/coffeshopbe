#!/bin/sh
# deploy.sh
# Easy deployment to multiple servers.
# Deploy code, files, settings and much more to multiple servers via ssh.

eval $(ssh-agent -s)
ssh-add /root/.ssh/gitlab/coffeeshop
git checkout release/staging && git pull